import React from 'react';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import Popover from "@material-ui/core/Popover";
import Button from "components/CustomButtons/Button.js";
import styles from "assets/jss/material-kit-react/popoverStyles.js";
import CategoriesFilters from "../Components/CategoryFilters"
import { addBook } from "../../api/endpoints"
const useStyles = makeStyles(styles);

export default function AddProductForm() {
    const classes = useStyles();
    const [anchorElBottom, setAnchorElBottom] = React.useState(null);
    const [categories, setCategories] = React.useState({});
    const [state, setState] = React.useState({

    });

    React.useEffect(async ()=>{
        /* //TODO connect to API
     const remoteData = await getCategories;
     const categories = JSON.parse(await remoteData.text());
     setCategories(categories);
        */
        return 1;

    },[])

    const handleChange = (event) => {
        setState({ ...state, [event.target.name]: event.target.value });
    };

    async function submitProduct() {
        //TODO connect to API
        const payload = state;
        await addBook(payload);
        console.log({...state})
    }

    return (
        <form className={classes.root} noValidate autoComplete="off" /*onSubmit={e=>console.log(e)}*/>
                <TextField
                    onChange={handleChange}
                    name="title"
                    id="book_name"
                    label="Title"
                    className={classes.textField}
                />
                <br/>
                <TextField onChange={handleChange}
                           name="author"
                           id="book_author"
                           label="Author"
                           className={classes.textField}  />
            <br/>
                <TextField onChange={handleChange}
                           name='pages'
                           id="book-pages"
                           label="Pages"
                           className={classes.textField} />
            <br/>
                <TextField onChange={handleChange}
                           name="price"
                           id="book-price"
                           label="Price"
                           className={classes.textField} />
            <br/>
            <br/>
            <TextField onChange={handleChange}
                       name="quantity"
                       id="book-quantity"
                       label="Quantity"
                       className={classes.textField} />
            <br/>
            <br/>
            <br/>
            <Button onClick={event => setAnchorElBottom(event.currentTarget)}>
                Categories
            </Button>
            <br/>
            <Popover
                classes={{
                    paper: classes.popover
                }}
                open={Boolean(anchorElBottom)}
                anchorEl={anchorElBottom}
                onClose={() => setAnchorElBottom(null)}
                anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "center"
                }}
                transformOrigin={{
                    vertical: "top",
                    horizontal: "center"
                }}
            >
                <h3 className={classes.popoverHeader}>Choose category</h3>
                <CategoriesFilters setCategory={e=>setCategories(e)} />
            </Popover>

                <br/>
                <br/>
                <br/>
            <Button color="primary" onClick={event => submitProduct()}>
                Add product
            </Button>
        </form>
    );
}
