import React, {useEffect} from 'react';
import Dialog from '@material-ui/core/Dialog';
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";
import Button from "components/CustomButtons/Button.js";
import { makeStyles } from "@material-ui/core/styles";
import Divider from '@material-ui/core/Divider';


const styles = (theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(2),
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing(1),
        top: theme.spacing(1),
        color: theme.palette.grey[500],
    },
    imgCard:{
        width: "100px"
    },
    bookDetails:{
        display: 'flex',
        justifyContent:'space-between'
    }
});

const useStyles = makeStyles(styles);
// const modalData = {
//     title, author, description, price, image
// }
export default function CustomizedDialogs({toggle,resetModal, selectedBook}) {
    const [open, setOpen] = React.useState(false);
    const [book, selectBook] = React.useState({
        title:'Book Title',
        author: 'author',
        price : 0
    })
    useEffect(()=>{
        setOpen(toggle)
    },[toggle])


    useEffect(()=>{

        selectBook(selectedBook)
    },[selectedBook])

    const handleClose = () => {
        resetModal(false)
    };

    const submitOrder = () => {
        let orders = localStorage.getItem('orders');
        if(!orders){
            orders =[ {
                item:JSON.stringify(selectedBook),
                quantity : 1,
            }]
            const payload = JSON.stringify(orders);
            localStorage.setItem('orders', payload);
        }
        else {
            let oldData = JSON.parse(orders);
            oldData.push({
                item: JSON.stringify(selectedBook),
                quantity : 1,
            })

            const payload = JSON.stringify(oldData);
            localStorage.setItem('orders', payload);
        }

        handleClose()
    }
    const classes = useStyles();

    return (
        <div>
            <Dialog PaperComponent="Card" fullScreen={true}
                    style={{backgroundColor:'rgba(0,0,0,0)',width:"80%", height:"90%", margin:'auto'}}
                    onClose={handleClose}
                    open={open}>
                <Card style={{width: "90%",
                    margin:'50px auto',
                    height: '100%'
                }}>
                    <CardHeader color="primary">{book.title}</CardHeader>
                    <CardBody>
                        <div className={classes.bookDetails}>
                            <img
                            style={{height: "250px", width: "auto", display: "inline-block"}}
                            className={classes.imgCard}
                            // src={product.image} //TODO set from API
                            src={require("../../assets/img/examples/mariya-georgieva.jpg")}
                            alt="Card-img-cap"
                            />
                            <div style={{width:"60%", paddingLeft: "40px"}}>
                                <h4 className={classes.cardTitle}>About this book</h4>
                                <h4>
                                    <small>Title: </small> <br/> {book.title}
                                    {/*<small>Title: </small> <br/> {product.title}//TODO set from API*/}
                                </h4>
                                <h4>
                                    <small>Author: </small> <br/> {book.author}
                                    {/*<small>Author: </small> <br/> {product.author}//TODO set from API*/}

                                </h4>
                            </div>
                        </div>
                        <Divider style={{margin :20}}/>
                        {/*<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed hendrerit ornare rutrum. Nunc ac sodales neque. Suspendisse at purus ullamcorper, consectetur orci sed, feugiat lorem. Phasellus sollicitudin, eros quis commodo ultrices, justo felis gravida enim, vel feugiat justo erat vel erat. Sed ultricies turpis a turpis viverra imperdiet. Quisque vitae viverra ante. Ut iaculis condimentum nisi eu iaculis.*/}

                        {/*    Mauris tortor enim, rhoncus a lacinia eget, congue sit amet odio. Integer fringilla commodo quam eget accumsan. In laoreet dui dolor, sit amet malesuada nisi pharetra eu. Donec et elit sapien. Aliquam erat volutpat. Mauris sit amet purus a felis sodales tincidunt. Fusce at dui nec turpis porttitor egestas. Mauris pharetra sem id nisl ullamcorper, sit amet porttitor lectus aliquet. Vivamus et arcu eu ipsum dictum interdum a nec velit. Sed nec iaculis tellus.*/}

                        {/*</p>*/}
                        <Divider style={{margin :20}}/>
                    </CardBody>
                    <div style={{padding : '20px', width:'100%'}}>
                        <h4>Price : <b>{book.price} EUR</b></h4>
                    <Button style={{ float:'right'}} autoFocus onClick={submitOrder} color="success">
                        Add to cart
                    </Button>
                    </div>
                </Card>
            </Dialog>
        </div>
    );
}
