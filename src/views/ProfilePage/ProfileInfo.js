import React from "react";
// material-ui components
import { makeStyles } from "@material-ui/core/styles";
// core components
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import IconButton from '@material-ui/core/IconButton';
import {Edit} from '@material-ui/icons';
import Divider from '@material-ui/core/Divider';

import { cardTitle, cardLink, cardSubtitle } from "assets/jss/material-kit-react.js";
import imagesStyles from "../../assets/jss/material-kit-react/imagesStyles";
import ProfileEditModal from "./ProfileEditModal";
const styles = {
    ...imagesStyles,
    cardTitle,
    cardLink,
    cardSubtitle,
    cardItem:{
        display:'flex',
        justifyContent:'space-between',
        alignItems: 'center'
    }
};

const useStyles = makeStyles(styles);

export default function UserInfoCard({user}) {
    const classes = useStyles();
    const [modal, toggleModal] = React.useState(false);
    return (
        <Card style={{width: "25rem"}}>
            <CardBody>
                <h4 className={classes.cardTitle}>Profile info</h4>
                <div className={classes.cardItem}>
                <h6 className={classes.cardSubtitle}>Full Name</h6>
                <p>
                    {user.name}
                </p>
                </div>
                <div className={classes.cardItem}>
                    <h6 className={classes.cardSubtitle}>Username</h6>
                    <p>
                        {user.username}
                    </p>
                </div>
                <div className={classes.cardItem}>
                    <h6 className={classes.cardSubtitle}>Email</h6>
                    <p>
                        {user.email}
                    </p>
                </div>
                <Divider/>
                <IconButton onClick={()=>toggleModal(!modal)} aria-label="delete">
                    <Edit />
                </IconButton>
            </CardBody>
            <ProfileEditModal onSubmit={(e)=>console.log(e)} toggle={modal}/>
        </Card>
    );
}
