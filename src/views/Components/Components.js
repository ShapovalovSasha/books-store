import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// react components for routing our app without refresh
// import { Link } from "react-router-dom";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// @material-ui/icons
// core components
import Header from "components/Header/Header.js";
import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
// import Button from "components/CustomButtons/Button.js";
import Parallax from "components/Parallax/Parallax.js";
// sections for this page
import HeaderLinks from "components/Header/HeaderLinks.js";
import ProductCard from "../ProductsPage/ProductCard";
import ProductModal from "../ProductsPage/ProductModal";
import styles from "assets/jss/material-kit-react/views/components.js";
import Card from "../../components/Card/Card";
import CategoriesFilters from "./CategoryFilters"
import { getBooks } from "../../api/endpoints"
const useStyles = makeStyles(styles);


export default function Components(props) {
  const classes = useStyles();
  const [productExpanded, expandProduct] = React.useState(false);
  const [selectedBook, selectBook] = React.useState({});
  function openProductModal(product){
      expandProduct(true);
      selectBook(product)
  }
  const [booksList,setBooks] = React.useState([]);
  // get books from api
  React.useEffect(()=>{
      getBooks().then(response=>{
          response.text().then(raw=>{
              const books= JSON.parse(raw);
              console.log(books)
              setBooks(books);
          })
      })
  },[])

  const { ...rest } = props;
  return (
    <div>
      <Header
        brand="Book store"
        rightLinks={<HeaderLinks />}
        fixed
        color="transparent"
        changeColorOnScroll={{
          height: 400,
          color: "white"
        }}
        {...rest}
      />
      <Parallax image={require("assets/img/main-pic.jpg")}>
        <div className={classes.container}>
          <GridContainer>
            <GridItem>
              <div className={classes.brand}>
                <h1 className={classes.title}>Book Store</h1>
                <h3 className={classes.subtitle}>
                “A room without books is like a body without a soul.”
                </h3>
              </div>
            </GridItem>
          </GridContainer>
        </div>
      </Parallax>
      <div className={classNames(classes.main, classes.mainRaised)}>

              <Card className={classes.categoriesColumn}>
                  <CategoriesFilters setCategory={e=>console.log(e)}/>
              </Card>
          <div className={classes.productsColumn}>
              {booksList.map((item)=>(
                  <ProductCard
                      key={item.id}
                      content={item.author}
                      title={item.title}
                      image={require("assets/img/main-pic.jpg")}
                      onButtonClick={(e)=>openProductModal(item)}
                  />
              ))}
      </div>
      </div>
        <ProductModal selectedBook={selectedBook} resetModal={()=>expandProduct(false)} toggle={productExpanded}/>
      <Footer />
    </div>
  );
}
