import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// @material-ui/icons

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";

import styles from "assets/jss/material-kit-react/views/landingPageSections/teamStyle.js";

import team1 from "assets/img/faces/avatar.jpg";
import team2 from "assets/img/faces/christian.jpg";
import team3 from "assets/img/faces/kendall.jpg";
import Divider from "@material-ui/core/Divider";

const useStyles = makeStyles(styles);

export default function TeamSection() {
  const classes = useStyles();
  const [cart, setCart] = React.useState([])
  const imageClasses = classNames(
    classes.imgRaised,
    classes.imgCardTop,
    classes.imgFluid
  );

  React.useEffect(()=>{
      const myOrders = localStorage.getItem('orders');
      if(myOrders) {
          const order = JSON.parse(myOrders);
          // localStorage.removeItem('orders')
          const realData = order.map(item => {
              item.item = JSON.parse(item.item);
              return item;
          })
          setCart(realData);
      }
  },[])
  return (
    <div className={classes.section}>
      <h2 className={classes.title}>Shopping cart</h2>
      <div>
        <GridContainer>
          <GridItem xs={12} sm={12} md={4} style={{margin:'auto'}} >
              {cart.map(item=>(
                  <div >
                  <Card plain>
                      <GridItem xs={12} sm={12} md={8} className={classes.itemGrid}>
                          <img src={team1} alt="..." className={imageClasses} />
                      </GridItem>
                      <h4 className={classes.cardTitle}>
                          {item.item.title}
                          <br />
                          <small className={classes.smallTitle}>{item.item.autor}</small>
                      </h4>
                      <CardBody>
                          <p className={classes.description}>
                              You can write here details about one of your team members. You
                              can give more details about what they do. Feel free to add
                              some for people to be able to follow them outside the site.
                          </p>
                      </CardBody>
                      <CardFooter className={classes.justifyCenter}>
                          <h4>
                              Price : <b>{item.item.price} EUR</b>
                          </h4>
                      </CardFooter>
                  </Card>
                      <Divider style={{ margin: 20 }} />
                  </div>
              ))}
          </GridItem>
        </GridContainer>
      </div>
      <div>
        <Divider style={{ margin: 20 }} />
        <Card plain className={classes.totalCard}>
            {cart.map(item=>(
                <div className={classes.cartItem}>
                    <h5>{item.item.title}</h5>
                    <h4>
                        <small>{item.item.price} EUR</small>
                    </h4>
                </div>
            ))}
            <div className={classes.cartItem}>
                <h3>Total </h3>
                <h3>
                    <small>{cart.reduce((prev,current)=>{return current.item.price + prev},0)} EUR</small>
                </h3>
            </div>
        </Card>
        <Divider style={{ margin: 20 }} />
      </div>
    </div>
  );
}
