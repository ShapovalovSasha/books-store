import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Email from "@material-ui/icons/Email";
import People from "@material-ui/icons/People";
// core components
import Header from "components/Header/Header.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";
import CardFooter from "components/Card/CardFooter.js";
import CustomInput from "components/CustomInput/CustomInput.js";

import styles from "assets/jss/material-kit-react/views/loginPage.js";

import image from "assets/img/bg7.jpg";
import TextField from "@material-ui/core/TextField";
import {register} from "../../api/endpoints";

const useStyles = makeStyles(styles);

export default function LoginPage(props) {
  const [cardAnimaton, setCardAnimation] = React.useState("cardHidden");
    const [state, setState] = React.useState({

    });
    const handleChange = (event) => {
        event.preventDefault();
        setState({ ...state, [event.target.name]: event.target.value });
    };
  setTimeout(function() {
    setCardAnimation("");
  }, 700);
  const classes = useStyles();
  const { ...rest } = props;
  async function registerUser(e){
      const form = e.currentTarget;
      const payload = {
          firstName : form['firstName'].value,
          lastName : form['lastName'].value,
          username : form['username'].value,
          password : form['password'].value,
          email : form['email'].value,
      }
     await register(payload);
      console.log(payload);
  }
  return (
    <div>
      <Header
        absolute
        color="transparent"
        brand="Material Kit React"
        rightLinks={<HeaderLinks />}
        {...rest}
      />
      <div
        className={classes.pageHeader}
        style={{
          backgroundImage: "url(" + image + ")",
          backgroundSize: "cover",
          backgroundPosition: "top center"
        }}
      >
        <div className={classes.container}>
          <GridContainer justify="center">
            <GridItem xs={12} sm={12} md={4}>
              <Card className={classes[cardAnimaton]}>
                <form onSubmit={registerUser} className={classes.form}>
                  <CardHeader color="primary" className={classes.cardHeader}>
                    <h4>Login</h4>
                  </CardHeader>
                  <CardBody>
                    <CustomInput
                      labelText="First Name..."
                      formControlProps={{
                        fullWidth: true
                      }}
                      inputProps={{
                        type: "text",
                          onChange:{handleChange},
                          name:"firstName",
                        endAdornment: (
                          <InputAdornment position="end">
                            <People className={classes.inputIconsColor} />
                          </InputAdornment>
                        )
                      }}
                    />
                      <CustomInput
                          labelText="Last Name..."
                          formControlProps={{
                              fullWidth: true
                          }}
                          inputProps={{
                              type: "text",
                              name:"lastName",
                              onChange:handleChange,
                              endAdornment: (
                                  <InputAdornment position="end">
                                      <People className={classes.inputIconsColor} />
                                  </InputAdornment>
                              )
                          }}
                      />
                      <CustomInput
                          labelText="Usename..."
                          formControlProps={{
                              fullWidth: true
                          }}
                          inputProps={{
                              onChange:{handleChange},
                              name:"username",
                              type: "text",
                              endAdornment: (
                                  <InputAdornment position="end">
                                      <People className={classes.inputIconsColor} />
                                  </InputAdornment>
                              )
                          }}
                      />
                    <CustomInput
                      labelText="Email..."
                      formControlProps={{
                        fullWidth: true
                      }}
                      inputProps={{
                        type: "email",
                          onChange:{handleChange},
                          name:"email",
                        endAdornment: (
                          <InputAdornment position="end">
                            <Email className={classes.inputIconsColor} />
                          </InputAdornment>
                        )
                      }}
                    />
                    <CustomInput
                      labelText="Password"
                      id="password"
                      formControlProps={{
                        fullWidth: true
                      }}
                      inputProps={{
                        type: "password",
                          onChange:{handleChange},
                          name:"password",
                        endAdornment: (
                          <InputAdornment position="end">
                            <Icon className={classes.inputIconsColor}>
                              lock_outline
                            </Icon>
                          </InputAdornment>
                        ),
                        autoComplete: "off"
                      }}
                    />
                  </CardBody>
                  <CardFooter className={classes.cardFooter}>
                    <Button type='submit' simple color="primary" size="lg">
                      Get started
                    </Button>
                  </CardFooter>
                </form>
              </Card>
            </GridItem>
          </GridContainer>
        </div>
        <Footer whiteFont />
      </div>
    </div>
  );
}
