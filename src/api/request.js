// import axios from 'axios';

export const request = (path, opts = {}) => {
    const headers = {
        ...opts.headers || {},
        'Content-Type': 'application/json',
        // 'token': sessionStorage.getItem( 'authToken' ) || localStorage.getItem( 'authToken' ) || ''

    };
    return fetch(
        path,
        { method: opts.method, ...opts, headers },
    ).then((result) => {
        if (result.status === 401) {
            // sessionStorage.setItem( 'authToken' , 'missing' );
            // window.location.assign( '/login' );
        }
        return result;
    });
};

// export const axiosRequest = (path, data) => axios.post(path, data, {
//     headers: { token: sessionStorage.getItem('authToken') },
// });
