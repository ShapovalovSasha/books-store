import { request } from './request.js';

// const url = 'http://192.168.0.234:8080';
const url = 'http://localhost:8080';

// LOGIN
export const signIn = (payload) => request(`${url}/api/auth/signin`, {
    method: 'POST',
    body: JSON.stringify(payload),
});

export const register = (payload) => request(`${url}/api/auth/signup`, {
    method: 'POST',
    body: JSON.stringify(payload),
});


// GET ALL BOOKS
export const getBooks= () => request(`${url}/api/books`, { method: 'GET' });

export const addBook = (payload) => request(`${url}/api/books`, {
    method: 'POST',
    body: JSON.stringify(payload),
});

export const updateBook = (payload) => request(`${url}/book/${payload.id}`, {
    method: 'PUT',
    body: JSON.stringify(payload),
});

