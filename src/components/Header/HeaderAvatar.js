import React from "react";
// material-ui components
import { makeStyles } from "@material-ui/core/styles";
import Popover from "@material-ui/core/Popover";
// core components
import Avatar from '@material-ui/core/Avatar';

import styles from "assets/jss/material-kit-react/popoverStyles.js";
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem, { ListItemProps } from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import {AccountCircle, Input, ShoppingCart} from '@material-ui/icons';
const useStyles = makeStyles(styles);

function handleClick(location) {
window.location.assign(`/${location}`);
}


export default function HeaderAvatar() {
    const classes = useStyles();
    const [anchorElLeft, setAnchorElLeft] = React.useState(null);
    const [anchorElTop, setAnchorElTop] = React.useState(null);
    const [anchorElBottom, setAnchorElBottom] = React.useState(null);
    const [anchorElRight, setAnchorElRight] = React.useState(null);
    return (
        <div>
            <IconButton style={{padding: 5}} onClick={event => setAnchorElBottom(event.currentTarget)}>
                <Avatar alt="Remy Sharp" src={require("../../assets/img/faces/avatar.jpg")} />
            </IconButton>
            <Popover
                classes={{
                    paper: classes.popover
                }}
                open={Boolean(anchorElBottom)}
                anchorEl={anchorElBottom}
                onClose={() => setAnchorElBottom(null)}
                anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "center"
                }}
                transformOrigin={{
                    vertical: "top",
                    horizontal: "center"
                }}
            >
                <h3 className={classes.popoverHeader}>Settings</h3>
                <List component="nav" aria-label="main mailbox folders">
                    <ListItem button onClick={()=>handleClick('profile-page')}>
                        <ListItemIcon>
                            <AccountCircle />
                        </ListItemIcon>
                        <ListItemText primary="My Profile" />
                    </ListItem>
                    <ListItem button onClick={()=>handleClick('shopping-cart')}>
                        <ListItemIcon>
                            <ShoppingCart />
                        </ListItemIcon>
                        <ListItemText primary="Shopping Cart" />
                    </ListItem>
                    <ListItem button onClick={()=>handleClick('login-page')}>
                        <ListItemIcon>
                            <Input />
                        </ListItemIcon>
                        <ListItemText primary="Logout" />
                    </ListItem>
                </List>
            </Popover>
        </div>
    );
}
